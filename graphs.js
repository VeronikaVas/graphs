var types = ['Pie', 'Line', 'Point', 'Bar'];

//Zadání
var description = "Počet bodů";
var values = [
    [4, 'Petr'],
    [5, 'Bára'],
    [2, 'Pepa']
];
var items = [, 'Honza', 'Bára', 'Lenka', 'Pepa'];
var selectedType = 'Line';

var elem = document.getElementById('myCanvas');

if (elem && elem.getContext) {
    var context = elem.getContext('2d');
    var width = elem.width;
    var height = elem.height;
    var margin = 30;
    var centerX = width / 2;
    var centerY = height / 2 + margin;

    if (context) {

        drawDescription(description, context, width, margin);

        if (selectedType == 'Pie') {
            var sum = 0;
            for (i = 0; i < values.length; i++) {
                sum += values[i][0];
            }
            var angle = 0;
            var r = Math.min(width / 3, height / 3);

            values.forEach(i => {
                var piece = 2 * Math.PI / sum * i[0];
                console.log(piece);
                drawSemicircle(context, centerX, centerY, r, angle, angle + piece);

                var x = centerX + r * 1.25 * Math.cos(angle + piece / 2);
                var y = centerY + r * 1.15 * Math.sin(angle + piece / 2);
                var text = i[0] + " (" + Math.round(100 * i[0] / sum) + "%)";
                context.fillStyle = '#000';
                context.fillText(text, x, y);
                context.fillText(i[1], x, y + 20);
                angle += piece;
            });

        } else {
            drawLine(context, margin, height - margin, width, height - margin, '#000');
            drawLine(context, margin, margin, margin, height - margin, '#000');


            var max = Math.ceil(Math.max.apply(Math, getValues(values)));
            var sizeY = (height - margin * 2 - 10) / max;
            for (i = 0; i < max; i++) {
                var y = height - margin - sizeY - (sizeY * i);
                drawLine(context, margin , y, width, y, '#d3d3d3');
                context.textAlign = 'center';
                context.fillText(i + 1, margin - 20, y + 2);
            }
        }

    }
}

function getValues(array){
    var a = [];
    values.forEach(i => {
        a.push(i[0]);
    });
    return a;
}


function drawDescription(description, context, width, margin) {
    context.fillStyle = '#000';
    context.font = 'bold 16px Arial';
    context.textAlign = 'center';
    context.fillText(description, width / 2, margin);
}

function drawSemicircle(context, x, y, radius, sangle, eangle) {
    context.fillStyle = randomColor();
    context.beginPath();
    context.moveTo(x, y);
    context.arc(x, y, radius, sangle, eangle);
    context.lineTo(x, y);
    context.fill();
    context.stroke();
}

function drawLine(context, startX, startY, endX, endY, color) {
    context.strokeStyle = color;
    context.beginPath();
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.closePath();
    context.stroke();
}

function randomColor() {
    return "hsl(" + 360 * Math.random() + ',' +
        (25 + 70 * Math.random()) + '%,' +
        (85 + 10 * Math.random()) + '%)';
}