class Graph {
    constructor(values, labels) {
        this.data = values;
        this.labels = labels;
        this.selected = [];
        this.canvas = undefined;
        this.description = undefined;
        this.legend = true;

        this.margin = 50;
        this.max = 0;
        this.sizeX = 0;
        this.sizeY = 0;

        this.mHeight = 0;
        this.mWidth = 0;

    }

    setCanvas(canvasName) {
        this.canvas = oCanvas.create({
            canvas: "#" + canvasName,
        });

        this.mHeight = this.canvas.height - this.margin;
        this.mWidth = this.canvas.width - this.margin;

        this.max = Math.ceil(Math.max.apply(Math, this.data));
        this.min = Math.floor(Math.min.apply(Math, this.data));

        if (this.min < 0) {
            this.sizeY = (this.canvas.height - this.margin * 2 - 10) / (this.max + Math.abs(this.min - 1));
        } else {
            this.sizeY = (this.canvas.height - this.margin * 2 - 10) / this.max;
        }

        this.sizeX = (this.canvas.width - this.margin * 3.5) / (this.labels.length);
    }

    setDescription(description) {
        this.description = description;
    }
    legendVisibility(legend) {
        this.legend = legend;
    }

    highlightItems(items) {
        this.selected = items;
    }

    drawDescription() {
        this.drawText(this.mWidth / 2, this.margin / 2, this.description, "bold 20px arial");
    }

    draw() {
        this.canvas.reset();
    }

    drawLine(sx, sy, ex, ey, lineStyle) {
        var line = this.canvas.display.line({
            start: {
                x: sx,
                y: sy
            },
            end: {
                x: ex,
                y: ey
            },
            stroke: lineStyle,
            cap: "round"
        });

        this.canvas.addChild(line);
        line.zIndex = "back";
    }

    drawText(x, y, t, font) {
        var text = this.canvas.display.text({
            x: x,
            y: y,
            origin: {
                x: "center",
                y: "center"
            },
            font: font,
            text: t,
            fill: "#000"
        });
        this.canvas.addChild(text);

    }

    drawBar(x, y, width, height, color) {
        var rectangle = this.canvas.display.rectangle({
            x: x,
            y: y,
            width: width,
            height: height,
            fill: color
        });

        this.canvas.addChild(rectangle);

    }

    drawAxes() {
        if (this.min > 0) {
            this.drawLine(this.margin, this.mHeight, this.mWidth, this.mHeight, "1px #000");
        } else {
            this.drawLine(this.margin, this.mHeight - (this.sizeY * Math.abs(this.min - 1)), this.mWidth, this.mHeight - (this.sizeY * Math.abs(this.min - 1)), "1px #000");
        }

        this.drawLine(this.margin, this.margin, this.margin, this.mHeight, "1px #000");
    }
}

class PointGraph extends Graph {
    draw() {
        super.draw();
        this.drawDescription();
        this.drawAxes();

        var color = "#0aa";

        for (var i = 0; i < this.max; i++) {
            var y = this.mHeight - this.sizeY - (this.sizeY * i);
            var x = this.margin + this.sizeX * (i + 1);
            this.drawLine(this.margin - 7, y, this.mWidth, y, '1px #d3d3d3');

            if (i < this.data.length) {
                this.drawLine(x, this.mHeight, x, this.margin, '1px #d3d3d3');
                if(this.legend){ this.drawText(x, this.mHeight + 15, this.labels[i], "15px arial"); }
                
                if (this.selected.includes(this.labels[i])) { color = "#005bd2";  } 
                else { color = "#0aa"}

                if(this.legend){
                    this.drawText(x, this.mHeight - (this.data[i] * this.sizeY) - 18, this.data[i], "18px " + color + " sans-serif");
                }
                
                var ellipse = this.canvas.display.ellipse({
                    x: x,
                    y: this.mHeight - (this.data[i] * this.sizeY),
                    radius: 5,
                    fill: color
                });

                this.canvas.addChild(ellipse);
                ellipse.zIndex = 999;
            }
            if(this.legend){ this.drawText(this.margin - 20, y + 2, i + 1, "15px arial");  }
        }
    }
}

class LineGraph extends PointGraph {
    draw() {
        super.draw();
        var tempX = this.margin;
        var tempY = this.mHeight;

        for (var i = 0; i < this.max; i++) {
            var y = this.mHeight - this.sizeY - (this.sizeY * i);
            var x = this.margin + (this.sizeX * i) + this.sizeX;

            if (i < this.data.length) {
                this.drawLine(tempX, tempY, x, this.mHeight - (this.data[i] * this.sizeY), '1px #0aa');
                tempX = x;
                tempY = this.mHeight - (this.data[i] * this.sizeY);
            }
        }
    }
}


class BarGraph extends Graph {
    draw() {
        super.draw();
        this.drawDescription();
        this.drawAxes();
        var color = "#0aa";
        var i = 0;
        for (var c = this.min; c < this.max + 1; c++) {
            var y = this.mHeight - (this.sizeY * c) - Math.abs(this.min - 1) * this.sizeY;
            var x = this.margin + (this.sizeX * c) + this.sizeX / 2;

            this.drawLine(this.margin - 7, y, this.mWidth, y, '1px #d3d3d3');
            if(this.legend) this.drawText(this.margin - 20, y + 2, c, "15px arial");

            if (i < this.data.length && this.legend ) {
                if (this.data[i] < 0) {
                    this.drawText(x - 0.55 * this.sizeX, this.mHeight - 15 - (this.sizeY * Math.abs(this.min - 1)), this.labels[i], "15px arial");
                    this.drawText(x - 0.55 * this.sizeX, this.mHeight - 60 + (this.data[i] * this.sizeY) + (this.sizeY * Math.abs(this.min - 1)),
                        this.data[i], "20px sans-serif" + color);
                } else {
                    if (c > 0) {
                        this.drawText(x - 0.55 * this.sizeX, this.mHeight + 15 - (this.sizeY * Math.abs(this.min - 1)), this.labels[i], "15px arial");
                        this.drawText(x - 0.55 * this.sizeX, this.mHeight - (this.data[i] * this.sizeY) - 15 - (this.sizeY * Math.abs(this.min - 1)),
                            this.data[i], "20px sans-serif" + color);
                    }
                }
            }

            if (this.selected.includes(this.labels[i])) { color = "#005bd2";} 
            else { color = "#0aa" }

            if (c > 0) {
                this.drawBar(x - 3.5 * this.sizeX / this.data.length,
                    this.mHeight - (this.sizeY * Math.abs(this.min - 1)) - (this.data[i] * this.sizeY),
                    this.sizeX / 1.5,
                    this.data[i] * this.sizeY, color);
                i++;
            }

        }
    }


}

class PieGraph extends Graph {
    draw() {
        super.draw();
        this.drawDescription();

        var pie = this.canvas.display.arc({
            x: this.canvas.width / 3,
            y: this.canvas.height / 2,
            radius: Math.min(this.mWidth / 2.5, this.mHeight / 2.5),
            pieSection: true
        });
        this.canvas.addChild(pie);
        var sum = this.data.reduce((a, b) => Math.abs(a) + Math.abs(b), 0);

        var pieces = [],
            start = 0,
            end = 0,
            lining = "2px #FFF";
            
        for (var i = 0; i < this.data.length; i++) {
            end = start + 360 / sum * Math.abs(this.data[i]);

            if (i == 0) lining = "4px #fff";
            else lining = "2px #fff" ;
            
            pieces.push(pie.clone({
                start: start,
                end: end,
                stroke: lining,
                fill: "hsl(195, " + (100 - i * 10) + "%, " + (50 - i * 10) + "%)"
            }));

            if (i == 0) {
                pieces[i].radius = Math.min(this.mWidth / 2.5, this.mHeight / 2.5) + 1;
            }

            this.canvas.addChild(pieces[i]);
            pieces[i].zIndex = "front";

            if(this.legend){
                this.drawBar(this.mWidth - this.margin * 3.8, this.mHeight / 4 + i * this.margin / 2, 30, 10, pieces[i].fill);
                this.drawText(this.mWidth - this.margin * 1.8, this.mHeight / 4 + i * this.margin / 2 + 5, this.labels[i] + " (" + this.data[i] + ")", "bold 15px sans-serif");
            }
            start = end;
        }
    }
}

var test = new BarGraph([
    5.6, 2.5, 7.4, 1
], ['Jana', 'Petr', 'Lucie', 'Katka']);
test.setCanvas("myCanvas");
test.setDescription("Skóre");
//test.legendVisibility(false);
test.highlightItems(["Petr"]);

test.draw();

 test = new PointGraph
([
    5.6, 2.5, 7.4, 1.5, 3, 4.6
], ['Jana', 'Petr', 'Lucie', 'Katka', 'Tereza', 'Marie']);
test.setCanvas("myCanvas2");
test.setDescription("Skóre");
test.highlightItems(["Jana", "Katka"]);
//test.legendVisibility(false);
test.draw();

 test = new LineGraph([
    5.6, 2.5, 7.4, 1, 3
], ['Jana', 'Petr', 'Lucie', 'Katka', 'Tereza']);
test.setCanvas("myCanvas3");
test.setDescription("Skóre");
test.highlightItems(["Petr"]);
test.draw();

test = new PieGraph([
    5.6, 2.5, 7.4, 1
], ['Jana', 'Petr', 'Lucie', 'Katka']);
test.setCanvas("myCanvas4");
test.setDescription("Skóre");
test.highlightItems(["Petr"]);
test.draw();